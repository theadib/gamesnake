#coding: latin1

'''
@author: Lars Heppert
'''

from OpenGL.GL import *
from OpenGL.GLU import *
import pygame, math, datetime
from pygame.locals import *

from snake import Snake
from playground import Playground

gameCycle       = 0
currentLevel    = 1
direction       = (0, 0)
commands        = []
playground      = Playground()
snake           = Snake(playground)

def resize(dimensions):
    if dimensions[1] == 0:
        dimensions[1] = 1
    glViewport(0, 0, dimensions[0], dimensions[1])
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    glOrtho(-10.0, playground.width * 10.0 + 20.0, playground.height * 10.0 + 20.0, -10.0, -6.0, 0.0)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

def init():
    glClearColor(0.0, 0.0, 0.0, 0.0)
                      
def clearScreen():
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glLoadIdentity()
    glTranslatef(0.0, 0.0, 3.0)
                        
def drawElement(element, color):
    r, g, b = color
    glColor3f(r, g, b)
    glBegin(GL_QUADS)
    for part in element:
        x, y    = part
        x       = x * 10.0
        y       = y * 10.0
        glVertex3f(x, y, 0.0)
        glVertex3f(9.0 + x, y, 0.0)
        glVertex3f(9.0 + x, 9.0 + y, 0.0)
        glVertex3f(x, 9.0 + y, 0.0)
    glEnd()

def handleEvent(event):            
    if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
        return False
    
    global commands
    if event.type == KEYDOWN:
        if event.key == K_RIGHT:
            commands.append(( +1,  0))
        if event.key == K_LEFT:
            commands.append(( -1,  0))
        if event.key == K_UP:
            commands.append((  0, -1))
        if event.key == K_DOWN:
            commands.append((  0, +1))
    
    return True
        
def showASCIIArt(fileName, delay):
    datei = open(fileName, "r")
    asciiArt = []
    y = -1
    for zeile in datei:
        x = -1
        y += 1
        for zeichen in zeile:
            if zeichen != '\n':
                x += 1
                if zeichen == 'X':
                    asciiArt.append((x, y))

    clearScreen()
    drawElement(asciiArt, (1.0, 0.0, 0.0))
    pygame.display.flip()
    pygame.time.delay(delay)
        
def showGameOver():
    showASCIIArt("GameOver.txt", 3000)
    restartGame()
        
def showYouWon():
    global currentLevel
    currentLevel += 1
    showASCIIArt("YouWon.txt", 5000)
    loadNextLevel(currentLevel)

def resetGameState():
    # reinitialize the game (restart)
    global direction, commands, playground, snake
    playground  = Playground()
    snake       = Snake(playground)    
    direction   = (0, 0)
    commands    = []
    pygame.event.clear()

def loadNextLevel(nextLevel):
    resetGameState()
    playground.loadLevel("level%d.txt" % nextLevel)

def restartGame():
    currentLevel = 1
    loadNextLevel(currentLevel)
        
def main():
    pygame.init()
    video_flags = OPENGL | HWSURFACE | DOUBLEBUF
    
    playground.loadLevel("level1.txt")    
    screenSize = (playground.width * 20, playground.height * 20)    
    pygame.display.set_mode(screenSize, video_flags)
    resize(screenSize)
    
    init()
    while True:
        if not handleEvent(pygame.event.poll()):
            break
        
        global gameCycle
        gameCycle += 1
        gameCycle %= 4
        if gameCycle == 0:
            global direction
            if commands:
                validCommand = False
                while not validCommand and commands:
                    new_x, new_y = commands.pop(0)
                    old_x, old_y = direction
                    if not ((abs(new_x) == abs(old_x)) and (abs(new_y) == abs(old_y))):
                        validCommand  = True
                
                if validCommand:
                    direction = (new_x, new_y)
            
            if direction != (0, 0):
                collisionEvent = snake.move(direction)
                if collisionEvent == Snake.COLLISION:
                    showGameOver()
                if collisionEvent == Snake.PASSEXIT:
                    if not playground.applePositions:
                        showYouWon()

            clearScreen()
            drawElement(playground.barPositions,    (0.5, 0.5, 0.5))
            drawElement(playground.applePositions,  (1.0, 0.0, 0.0))
            drawElement(playground.snakePosition,   (0.0, 1.0, 0.0))
            if not playground.applePositions:
                drawElement(playground.exitPosition,    (0.0, 0.0, 1.0))
            
                
            pygame.display.flip()
        
        pygame.time.delay(50)

if __name__ == '__main__':
    main()
