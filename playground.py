#coding: latin1

'''
@author: Lars Heppert
'''

from snake import Snake

class Playground(object):
    '''
    represents the playground in our snake game
    '''

    def __init__(self):
        self.width          = 0
        self.height         = 0
        self.applePositions = []
        self.barPositions   = []
        self.exitPosition   = []
        self.snakePosition  = []
    
    def loadLevel(self, fileName):
        datei = open(fileName, "r")
        y = -1
        for zeile in datei:
            x = -1
            y += 1
            for zeichen in zeile:
                if zeichen != '\n':
                    x += 1
                    if zeichen == 'A':
                        self.applePositions.append((x, y))
                    elif zeichen == 'S':
                        self.snakePosition.append((x, y))
                    elif zeichen == 's':
                        self.snakePosition.insert(0, (x, y))
                    elif zeichen == 'H':
                        self.barPositions.append((x,y))
                    elif zeichen == 'E':
                        self.exitPosition.append((x,y))
            
        self.width  = x
        self.height = y
        print(self.width, " -> ", self.height)

    def collisionDetection(self, newHeadPosition):
        if newHeadPosition in self.snakePosition:
            print("self collision")
            return Snake.COLLISION
        
        if newHeadPosition in self.barPositions:
            print("bar collision")
            return Snake.COLLISION
        
        if newHeadPosition in self.applePositions:
            print("apple collision")
            self.applePositions.remove(newHeadPosition)
            return Snake.EATAPPLE
            
        if newHeadPosition in self.exitPosition:
            print("exit collision")
            return Snake.PASSEXIT
        
        return Snake.NOCOLLISION
