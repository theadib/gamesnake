#coding: latin1

'''
@author: Lars Heppert
'''

class Snake(object):
    '''
    represents a snake in our snake game
    '''
    NOCOLLISION =  0
    COLLISION   = -1
    EATAPPLE    = -2
    PASSEXIT    = -3

    def __init__(self, playground):
        self.playground = playground
        self.expansion = 0
        
    def move(self, direction):
        x, y = self.playground.snakePosition[0]
        dx, dy = direction
        newHeadPosition = (x + dx, y + dy)
        
        collisionEvent = self.playground.collisionDetection(newHeadPosition)
        if collisionEvent == Snake.EATAPPLE:
            self.expansion += 4
    
        self.playground.snakePosition.insert(0, newHeadPosition)    
        if self.expansion == 0:
            self.playground.snakePosition.pop()
        else:
            self.expansion -= 1
            
        return collisionEvent